# Objetivo

Este proyecto contiene una serie de pruebas que serán ejecutadas
para verificar la consistencia del proyecto realizado durante
el semestre en el curso de sistemas operativos ST0257-2013-1.

# Requisitos

    g++ >= 4.6.3

# Instalacion

Simplemente ejecute 

    $ make install

Este creara un directorio llamado bin donde estarán ubicados los programas
que serán utilizados por los programas ubicados en el directorio pruebas.
