/* operar.cpp Este programa tiene como propósito leer de la entrada estándar
 * una secuencia de enteros y realizar con ellos una operación (sumar,
 * restar, multiplicar y dividir) determinada
 * por el usuario de la aplicación y retorna cada valor leído con la operación
 * aplicada.
 */
#include <iostream>
#include <cstdlib>
#include <getopt.h>
#include <unistd.h>

using namespace std;

// Declaración de las operaciones que van a implementadas
int sumar(int, int);
int restar(int, int);
int mult(int, int);
int dividir(int, int);

typedef int (*funcion)(int,int);

// Función que muestra el uso del programa y termina. 
void
usage(char *progname) {
  cerr << "Uso: " << *progname 
       << " [(-o|--oper) ('+'|'-'|'*'|'/')]"
       << " [(-t|--tiempo) int)]" 
       << " [(-v|--valor) int]"
       << endl;
  ::exit(1);
}

/* 
 * El programa tiene dos partes: en primer lugar lee las opciones permitidas
 * para el mismo: --tiempo int  | -t int
 *                --valor  int  | -v int
 *                --oper   char | -o char
 * El tiempo sirve para cuanto tiempo (en seg) espera antes de 
 * hacer la operacion seleccionada. Por omisión el tiempo es 0.
 * El valor es el valor a realizar la operacion. Por omisión el valor es 1.
 * Oper es un char: '+', '-', '*', '/'.
 * La segunda parte es hacer la operación. Lo importante aquí es la estructura
 * datos struct valores. Esta lleva la información del tiempo, el valor de 
 * de la operación y la función. Esta última es la importante es un puntero
 * a una de cuatros funciones con la siguiente signatura: int f(int, int).
 */
int
main(int argc, char *argv[]) {
  int c;
  int digit_optind = 0;
  int option_index = 0;
  char *progname = argv[0];
  static struct valores {
    int tiempo;
    int valor;
    funcion oper;
  } valores = { 0, 1, sumar};
  static struct option long_options[] = {
    {"tiempo", 1, NULL, 't'},
    {"valor",  1, NULL, 'v'},
    {"oper", 1, NULL, 'o'},
    {NULL, 0, NULL, 0}
  };

  // Primera parte verifica los argumentos de entrada
  while ((c = ::getopt_long(argc, argv, "o:t:v:",
			    long_options, &option_index)) != -1) {
    switch (c) {
    case 'o':

      switch(optarg[0]) {
      case '+':
	valores.oper = sumar;
	break;
      case '-':
	valores.oper = restar;
	break;
      case '*':
	valores.oper = mult;
	break;
      case '/':
	valores.oper = dividir;
	break;
      default:
	usage(progname);
	break;
      }
    case 't':
      valores.tiempo = ::atoi(optarg);
      break;
    case 'v':
      valores.valor = ::atoi(optarg);
      break;
    default:
      usage(progname);
      break;
    }
  }

  // Segunda parte: lee de la entrada estándar una secuencia de 
  // numeros y le aplica la operación definida en la estructura de valores.
  int x;

  while (cin >> x) {

    if (valores.tiempo) ::sleep(valores.tiempo);

    cout << valores.oper(x,valores.valor) << endl;
  }

  return 0;
}

// Implementación de las operaciones.

int
sumar(int a, int b) {
  return a + b;
}

int
restar(int a, int b) {
  return a - b;
}

int
mult(int a, int b) {
  return a * b;
}

int
dividir(int a, int b) {
  return a / b;
}
