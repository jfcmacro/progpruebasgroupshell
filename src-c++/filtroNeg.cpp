#include <iostream>
#include <cstdlib>

using namespace std;

int
main(int argc, char *argv[]) {

  int x;
  while (cin >> x) {
    if (x < 0) {
      cout << x << endl;
    }
  }

  return 0;
}
