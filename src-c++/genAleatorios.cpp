// genAleatorios.cpp 
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <climits>

using namespace std;

void
usage(char *progname) {
  cerr << progname 
       << " veces " << endl; 
  exit(1);
}

int
genNumber() {
  return (int) (random() % INT_MAX);
}

int
main(int argc, char *argv[]) {

  int desde = 0;
  int hasta;

  srandom(time(NULL));

  if (argc != 2) {

    usage(argv[0]);
  }
  else {

      hasta = atoi(argv[1]);
  }

  for (; desde < hasta; desde++) {
    cout << genNumber()  << endl;
  }
}
