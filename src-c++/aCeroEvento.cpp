/*
 * aCeroEvento.cpp Este programa tiene como propósito leer
 * de la entrada estándar una secuencia de enteros y realizar
 * transformalos a cero. Pero mientras realiza la operación espera
 * por alguna señal especifica un número determinado de veces, después
 * de las cuales el programa termina.
 */
#include <iostream>
#include <cstdlib>
#include <cerrno>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>

using namespace std;

void
usage(char *);

void
manejadorSenal(int);

static struct cfg {
  int senal;
  int veces;
  int espera;
} cfg = { SIGINT, 1, 0 };

static struct sigaction nuevaAct;
static struct sigaction viejaAct;

int
main(int argc, char *argv[]) {

  char *progname;
  int digit_optind = 0;
  int option_index = 0;
  static struct option long_options[] = {
    {"senal", 1, NULL, 's'},
    {"veces", 1, NULL, 'v'},
    {"espera", 1, NULL, 'e'},
    {NULL, 0, NULL, 0}
  };

  progname = argv[0];

  int c;
  while ((c = ::getopt_long(argc, argv, "s:v:e:",
			    long_options, &option_index)) != -1) {
    switch (c) {
    case 's':
      cfg.senal = ::atoi(optarg);
      break;
    case 'v':
      cfg.veces = ::atoi(optarg);
      break;
    case 'e':
      cfg.espera = ::atoi(optarg);
      break;
    default:
      usage(progname);
      break;
    }
  }

  nuevaAct.sa_handler = manejadorSenal;
  sigemptyset(&nuevaAct.sa_mask);
  nuevaAct.sa_flags = 0;

  ::sigaction(cfg.senal, &nuevaAct, &viejaAct);

  int x;
  do {
    while (cin >> x) {

      if (cfg.espera) sleep(cfg.espera);
      cout << 0 << endl;
    }
  } while (errno == EINTR);

  return 0;
}

void
usage(char *progname) {

  cerr << "Uso: " << progname <<
    " (-s int | --senal int) (-e int | --espera int) (-v int | --veces int)"
       << endl;
  exit(0);
}

void
manejadorSenal(int senal) {
  cfg.veces;
  if (cfg.veces == 0) {
    exit(1);
  }
  else {
    // reinstala el manejador de senal
    ::sigaction(cfg.senal, &nuevaAct, &viejaAct);
  }
  cfg.veces--;
  return;
}
