
all: pruebas

.phony: pruebas install clean

pruebas:
	$(MAKE) --directory=src-c++

install:
	if [ ! -d bin ]; then \
	   mkdir bin;   \
	fi
	$(MAKE) --directory=src-c++ install

clean:
	rm -f -R bin
	$(MAKE) --directory=src-c++ clean